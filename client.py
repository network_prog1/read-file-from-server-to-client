import socket

# Configuration
server_host = 'angsila.informatics.buu.ac.th'
server_port =  13085

# Create socket
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect to server
client_socket.connect((server_host, server_port))

# Receive file contents
# file_contents = b''
# while True:
#    data = client_socket.recv(1024)
#    if not data:
#        break
#    file_contents += data

with open('received_file.txt', 'wb') as file:
    while True:
        data = client_socket.recv(1024)
        if not data:
            break
        file.write(data)

# Close socket
client_socket.close()

# Print file contents
print("File received and saved as 'received_file.txt'")

with open('received_file.txt', 'r') as file:
    file_contents = file.read()

print(file_contents)





# import socket

# msgFromClient       = "Hello UDP Server"
# bytesToSend         = str.encode(msgFromClient)
# serverAddressPort   = ("127.0.0.1", 20001)
# bufferSize          = 1024

# # Create a UDP socket at client side
# UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

# # Send to server using created UDP socket
# UDPClientSocket.sendto(bytesToSend, serverAddressPort)

# msgFromServer = UDPClientSocket.recvfrom(bufferSize)
# msg = "Message from Server {}".format(msgFromServer[0])
# print(msg)