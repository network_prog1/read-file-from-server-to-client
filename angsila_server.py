import socket

# Configuration
# host = '0.0.0.0'  # Listen on all available interfaces
host = 'angsila.informatics.buu.ac.th'
port = 13085     # Choose a suitable port

# Read contents from the file
with open('/home/BUU/63160066/network_prog/a.txt', 'rb') as file:
        file_contents = file.read()

# Create socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind and listen
server_socket.bind((host, port))
server_socket.listen()

print(f"Server listening on {host}:{port}")

# Accept incoming connection
client_socket, client_address = server_socket.accept()
print(f"Accepted connection from {client_address}")

# Send file contents to client
client_socket.sendall(file_contents)

# Close socket
client_socket.close()
server_socket.close()





# import socket

# localIP     = "127.0.0.1"
# localPort   = 20001
# bufferSize  = 1024

# msgFromServer       = "Hello UDP Client"
# bytesToSend         = str.encode(msgFromServer)

# # Create a datagram socket
# UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

# # Bind to address and ip
# UDPServerSocket.bind((localIP, localPort))

# print("UDP server up and listening")

# # Listen for incoming datagrams
# while(True):
#     bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
#     message = bytesAddressPair[0]
#     address = bytesAddressPair[1]

#     clientMsg = "Message from Client:{}".format(message)
#     clientIP  = "Client IP Address:{}".format(address)
#     print(clientMsg)
#     print(clientIP)

#     # Sending a reply to client
#     UDPServerSocket.sendto(bytesToSend, address)
#     UDPServerSocket.close()
#     break






# while True:
#     # รอการเชื่อมต่อจาก client
#     print("waiting for connection")
 
#     # รับการเชื่อมต่อจาก client
#     connection, client_address = server_socket.accept()
#     try:
#         print("connection from", client_address)
 
#         # รับข้อมูลจาก client
#         while True:
#             # กำหนดขนาดข้อมูลที่จะรับใน recv()
#             data = connection.recv(1024)
#             print("received:", data)
 
#             # ถ้ามีข้อมูลส่งเข้ามาให้ส่งกลับไปหา client
#             if data:
#                 print("sending data back to the client")
#                 connection.sendall(data)
            
#             # ถ้าไม่มีข้อมูลให้จบการรอรับข้อมูล
#             else:
#                 print("no more data from", client_address)
#                 break
    
#     # รับข้อมูลเสร็จแล้วทำการปิดการเชื่อมต่อ
#     finally:
#         connection.close()
#         print("closed connection")
#         break


